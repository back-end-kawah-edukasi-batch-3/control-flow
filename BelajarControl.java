public class BelajarControl {
    public static void main(String[] args) {
        
        int angka = 21;

        if (angka > 15 && angka < 20) {
    	    System.out.println("Angka lebih besar dari 15 lebih kecil dari 20");
        } else if (angka > 15 || angka < 20){
            System.out.println("Lebih besar dari 15 Atau Lebih Kecil Dari 20");
        } else {
            System.out.println("Angka Sama Dengan 20");
        }

        System.out.println("Program Selesai");

    }
}
