public class BelajarSwitch {
    public static void main(String[] args) {
        
        int angka = 19;
        String penampung = "Ini Kosong";

        System.out.println(penampung);

        switch (angka) {
            case 15:
            penampung = "ini Lima Belas";
            break;
            case 16:
            penampung = "ini Enam Belas";
            break;
            case 17:
            penampung = "Ini Tujuh Belas";
            break;
            default:
            penampung = "Ini Bukan 15,16 Ataupun 17";
        }

        System.out.println(penampung);

    }
}
